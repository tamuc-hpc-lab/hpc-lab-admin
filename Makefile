INSTALL_DIR=/usr/local/bin

clean:
	find . -name "*.pyc" -print

install:
	cp scripts/check-node-status.py ${INSTALL_DIR}/lion-status
	chmod ugo+rx ${INSTALL_DIR}/lion-status
	cp scripts/user-quota.py ${INSTALL_DIR}/lion-user-quota
	chmod ugo+rx ${INSTALL_DIR}/lion-user-quota
	cp scripts/g09sub.sh ${INSTALL_DIR}/g09sub
	chmod ugo+rx ${INSTALL_DIR}/g09sub

