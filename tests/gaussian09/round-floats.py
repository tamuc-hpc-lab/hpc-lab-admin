#!/usr/bin/env python
"""Simple script, uses a regular expression to find all floating point numbers
and then round the values do D decimal places.
"""
import fileinput
import re

N = 5


def round_replace(match):
    """Return the floating point number, but rounded to N digits.
    We expect the matched float in the match.group(1), as a string.  We convert to float,
    use built in round method, then convert back to string for replacement."""
    f = float("%se%s" % (match.group(1), match.group(2)))
    if abs(f) < 1.0e-10:
        f = 0.0
    return "%e" % (round(f,N))


if __name__ == "__main__":

    for line in fileinput.input():
        p = re.compile('([\+\- ]\d+\.\d+)D([\+|-]\d+)')
        l = p.sub(round_replace, line)
        print l[:-1]


