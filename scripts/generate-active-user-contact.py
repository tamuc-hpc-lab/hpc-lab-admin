#!/usr/bin/env python
"""
Using the information field in the /etc/passwd file, generate a csv of active users 
and their contact information
"""
import fileinput

PASSWD_FILE='/etc/passwd'

if __name__ == "__main__":
    # print header
    print "Name, Acct Type, UID, username, Department, Office, Phone, e-mail, Create Date"

    # parse password file for information
    for line in fileinput.input(PASSWD_FILE):
        (username, passwd, uid, gid, info, home, shell) = line.split(':')
        uid = int(uid)
        if uid >= 1001 and uid < 2000 and uid != 1002: # uid 1002 kludge, currently gaussian system account
            name, dept, office, phone, email, create_date = info.split(',')
            acct_type = name.split(')')[0]
            name = name.split(')')[1]
            print "%s, %s, %d, %s, %s, %s, %s, %s, %s" % (name, acct_type, uid, username, dept, office, phone, email, create_date)

