#!/usr/bin/env python
"""
Wrapper script for submitting gaussian jobs.  Given name of a com file,
create a working qsub file, and submit the job to the sge grid engine
queue
"""
import fnmatch
import os
import os.path
import sys


if __name__ == "__main__":
    # we require two arguments, the name of the gaussian job com simulation file
    # and the number of nodes to run the job on.
    if len(sys.argv) != 3:
        print "Usage: %s gauss_job num_nodes" % sys.argv[0]
        print "   gauss_job: The name of the .com file holding the gaussian simulation job"
        print "              parameters.  The file extension, like .com, is optional"
        print "   num_nodes: Number of nodes to disribute the gaussian job over. Includes"
        print "              both the master and all Linda workers in the total."
        sys.exit(0)

    # parse job name from argv1, find base file name if a .com or .log name was given
    job_name = sys.argv[1]
    if job_name.rfind('.') >= 0:
        i = job_name.rfind('.')
        job_name = job_name[:i]
    com_name = job_name + ".com"
    log_name = job_name + ".log"

    if not job_name[0].isalpha():
        queue_name = "G" + job_name
    else:
        queue_name = job_name

    # parse number of nodes from argv2, make sure it is a valid integer
    num_nodes = int(sys.argv[2])

    # find location of the com file
    matches = []
    for root, dirnames, filenames in os.walk('.'):
        for filename in fnmatch.filter(filenames, com_name):
            matches.append(os.path.join(root, filename))

    if len(matches) == 0:
        print "gaussian job simulation file not found, no file named: ", com_name
        sys.exit(0)

    if len(matches) > 1:
        print "gaussian job simulation file name not unique, found multiple .com files: ", matches
        sys.exit(0)

    # change directory to location of .com file
    dir_loc = os.path.dirname(matches[0])
    os.chdir(dir_loc)

    # create the qsub file from provided information
    qsub = \
"""
#!/bin/bash
#
#$ -cwd
#$ -j y
#$ -S /bin/bash
#$ -N %s
#$ -l mem_free=11G
#$ -l excl=true

# Determine com and log file names
JOB_NAME=%s
COM_NAME=$JOB_NAME.com
LOG_NAME=$JOB_NAME.log

# Prepare for running Gaussian '09
export GAUSS_LFLAGS=' -vv -opt "Tsnet.Node.lindarsharg: ssh"'
LINDAWORKERS=$(cat $PE_HOSTFILE | grep -v "catch_rsh" | awk -F '.' '{print $1}' | tr '\n' ',' | sed 's/,$//')
echo ${LINDAWORKERS}

# Prepend input deck with necessary information to run Gaussian '09 with Linda 8.2
# Run Gaussian '09
date
(echo %%nprocshared=8; echo %%lindaworkers=${LINDAWORKERS}; echo %%mem=6GB; cat ${COM_NAME} | grep -v "%%nprocshared" | grep -v "%%lindaworkers" | grep -v "%%mem") | \
  /share/apps/g09/g09 > ${LOG_NAME} 
date
""" % (queue_name, job_name)
    qsub_name = '%s.qsub' % (job_name)
    fd = open(qsub_name, 'w')
    fd.write(qsub)
    fd.close()

    # now run the qsub command
    cmd = "qsub -pe orte %d %s" % (num_nodes, qsub_name)
    print "Submitting com job file: ", com_name
    print "        number of nodes: ", num_nodes
    print "          log file name: ", log_name
    print "              qsub name: ", queue_name
    print "      working directory: ", os.getcwd()
    print "                command: ", cmd
    ret_code = os.system(cmd)
    if ret_code == 0:
        print "\033[32mSubmitted successfully\033[39m"
    else:
        print "\033[31mError, job failed\033[39m"
