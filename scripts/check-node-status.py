#!/usr/bin/env python
"""Check the current status of all cluster compute and nas managed nodes,
and print out report to stdout in readable, easy to use format"""
import subprocess
import string
import re
import sys

MANAGEMENT_NODES = ['lionhpc', 'lionnas']
COMPUTE_NODE_BASENAME = "lion"
NUM_RACKS = 2
NUM_COMPUTE_NODES_PER_RACK = 32
LOAD_THRESHOLD_LOW = 4.0
LOAD_THRESHOLD_MED = 8.0
DISK_THRESHOLD_LOW = 33
DISK_THRESHOLD_MED = 66 


def create_node_list():
    """Create list of nodes of which we will check status.  We code this rather
    than using rocks list hosts because we want to check status of nodes even
    if they have not been configured properly and are under Rocks management
    """
    nodes = MANAGEMENT_NODES
    for rack in range(1, NUM_RACKS+1):
        for node_num in range(1, NUM_COMPUTE_NODES_PER_RACK+1):
            node_name = '%s-%d-%d' % (COMPUTE_NODE_BASENAME, rack, node_num)
            nodes.append(node_name)
    return nodes


def check_node_status(nodes):
    """Use ssh command to determine status of node, format and print
    results to stdout for readability.
    """
    # display header
    print '%10s  %6s %10s %10s     %05s %05s %05s %23s %4s %7s %7s' % ('Hostname', 'Status', 'Uptime', 'Users', '1min', '5min', '15min', 'Size  Used Avail Use%', 'Ncpu', 'Mem_Tot', 'Mem_use') 


    # poll nodes and display their status
    for node in nodes:

        # get up/down status and load information
        cmd = ['ssh', '-oBatchMode=yes', node, 'uptime']
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out,_none_ = p.communicate()
        status = p.returncode
        fields = string.split(out, ',')
        # return status 0 means successuly ran command
        if status == 0: 
            match = re.search('.*up (\d+ min|\d+ days?, .*|.*),.*?(\d+ users?),.*?(\d+\.\d+),.*?(\d+\.\d+),.*?(\d+\.\d+).*', out)

            if match:
                uptime = match.group(1)
                if uptime.find(','):
                    uptime = uptime.split(',')[0]
                users, load_1, load_5, load_15 = match.group(2), float(match.group(3)), float(match.group(4)), float(match.group(5))
                # check for load load
                if load_1 < LOAD_THRESHOLD_LOW and load_5 < LOAD_THRESHOLD_LOW and load_15 < LOAD_THRESHOLD_LOW:
                    print '%10s: up     %10s %10s    \033[32m %5.2f %5.2f %5.2f\033[39m' % (node, uptime, users, load_1, load_5, load_15),
                elif load_1 < LOAD_THRESHOLD_MED and load_5 < LOAD_THRESHOLD_MED and load_15 < LOAD_THRESHOLD_MED:
                    print '%10s: up     %10s %10s    \033[33m %5.2f %5.2f %5.2f\033[39m' % (node, uptime, users, load_1, load_5, load_15),
                else:
                    print '%10s: up     %10s %10s    \033[31m %5.2f %5.2f %5.2f\033[39m' % (node, uptime, users, load_1, load_5, load_15),
            else: # couldn't match regex, should fix
                print '%10s: up     %s' % (node, out[:-1]),
        else:
            print '%10s: \033[31mDOWN\033[39m' % (node),
            pass
	
        # get disk usage infromation
        if node == 'lionnas':
            disk = '/export'
        else:
            disk = '/state/partition1'
        cmd = ['ssh', '-oBatchMode=yes', node, 'df', '-h', disk]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out,_none_ = p.communicate()
        status = p.returncode
        # return status 0 means successfully ran command
        if status == 0: 
            match = re.search('(\d+\.?\d*[T|G|M]? +\d+\.?\d*[T|G|M]? +\d+\.?\d*[T|G|M]? +\d+\%)', out)
            usage = match.group(1)
            pct = int(usage.split()[3][:-1])
            if pct < DISK_THRESHOLD_LOW:
                print "\033[32m%23s\033[39m" % (usage),
            elif pct < DISK_THRESHOLD_MED:
                print "\033[33m%23s\033[39m" % (usage),
            else:
                print "\033[31m%23s\033[39m" % (usage),
        else:
            print "" 
        
	# get cpu usage and memory infromation
	cmd = ['qhost', '-h', node]
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	out,_none_ = p.communicate()
	status = p.returncode
        # return status 0 means successuly ran command
	if status == 0:
 	    match = re.search('(\d+) *\d+\.\d+ *(\d+\.\d+[G|M]) *(\d+\.\d+[G|M])', out)
            if match:
	        ncpu = match.group(1)
                memtot = match.group(2) 
		memuse = match.group(3)
                print " %3s %7s %7s " % (ncpu, memtot, memuse)
            else:
                print ""  
        else:
            print '%10s: \033[31mDOWN\033[39m' % (node)
            pass


if __name__ == '__main__':

    nodes = create_node_list()
    check_node_status(nodes)

