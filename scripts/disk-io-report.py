#!/usr/bin/env python
"""Generate a report on disk I/O performance for all cluster compute nodes."""
import subprocess


MANAGEMENT_NODES = ['lionhpc', 'lionnas']
COMPUTE_NODE_BASENAME = "lion"
NUM_RACKS = 2
NUM_COMPUTE_NODES_PER_RACK = 32

def create_node_list():
    """Create list of nodes of which we will check status.  We code this rather
    than using rocks list hosts because we want to check status of nodes even
    if they have not been configured properly and are under Rocks management
    """
    nodes = MANAGEMENT_NODES
    for rack in range(1, NUM_RACKS+1):
        for node_num in range(1, NUM_COMPUTE_NODES_PER_RACK+1):
            node_name = '%s-%d-%d' % (COMPUTE_NODE_BASENAME, rack, node_num)
            nodes.append(node_name)
    return nodes


def check_node_io(nodes):
    num_trials = 5
    bs = 512
    count = 100
    oflag = "all"
    script = "/home/clustertester/repos/hpc-lab-admin/scripts/test-disk-io.py"

    conds = ["NFS", "SCRATCH"]
    fs_locs = ["/home/clustertester", "/state/partition1"]

    for node in nodes:
        for cond_i in range(len(conds)):
            cond = conds[cond_i]
            fs_loc = fs_locs[cond_i]

            cmd = ['ssh', '-oBatchMode=yes', node, "%s" % script, "%d" % num_trials, "%d" % bs, "%d" % count, oflag, fs_loc]
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            out, _none_ = p.communicate()
            status = p.returncode
            # return status 0 means successfully ran
            if status == 0:
                for line in out[:-1].split('\n'):
                    print "%s, %s, %s" % (line[:-1], node, cond)


if __name__ == '__main__':

    nodes = create_node_list()
    check_node_io(nodes)
