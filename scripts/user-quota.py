#!/usr/bin/env python
"""
The quota command is a bit confusing for normal users.  Display quota information for username
provided as input on the command line.  Convert information to gigabyte/terabyte human readable
output.
"""
import os
import pwd
import re
import subprocess
import sys


if __name__ == "__main__":
    # use first command line argument for username, or use current username if not provided
    if len(sys.argv) == 2:
        username = sys.argv[1]
    else:
        username = pwd.getpwuid(os.getuid())[0]

    # run quota command on the username
    cmd = ['quota', username]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    out, _none_ = p.communicate()

    lines = out.split('\n')

    # check for user with no disk quota
    match = re.search('none', lines[0])
    if match: # user has no disk quota
        print lines[0]
    else: # user has quotas
        print lines[0]
        print "     %-15s  %10s  %10s  %10s" % ('Filesystem', 'Used', 'Soft Limit', 'Hard Limit')
        # convert 1k block information to G or T information
        match = re.search('\s*(\d+)\s*(\d+)\s*(\d+)\s*(\d+)\s*(\d+)\s*(\d+)\s*', lines[3])
        if match:
            cur_blocks_used = float(match.group(1))
            cur_gb_used = cur_blocks_used / 1024.0 / 1024.0

            soft_blocks_limit = float(match.group(2))
            soft_gb_limit = soft_blocks_limit / 1024.0 / 1024.0

            hard_blocks_limit = float(match.group(3))
            hard_gb_limit = hard_blocks_limit / 1024.0 / 1024.0

            print "     %-15s  %9.1fG  %9.1fG  %9.1fG" % ('/home/'+username, cur_gb_used, soft_gb_limit, hard_gb_limit)

        else:
            print "Error: could not find quota information"
