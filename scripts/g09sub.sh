#!/usr/bin/env bash
# Shell script to automate and make easier submission of gaussian '09 simulations
# to the SGE batch queue.  Given name of a com file, and number of nodes to submit
# the job 2, create a working qsub file, and submit the job to the SGE grid batch
# queue.

# we require 2 arguments, the name fo the gaussian job com simulation file and
# the number of nodes to run the job on.
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 gauss_job num_nodes"
  echo "    gauss_job: The name of the .com file holding the gaussian simulation job"
  echo "               parametetrs.  The file extension, like .com, is optional"
  echo "    num_nodes: Number of nodes to distribute the gaussian job over.  Includes"
  echo "               both the master and all Linda workers in the total."
  exit 0
fi

job_name=$1
num_nodes=$2

# parse job name from argv1, find base file name if a .com or .log name was given
job_name="${job_name%.*}"
com_name="$job_name.com"
log_name="$job_name.log"
case $job_name in
  [A-Za-a]*) queue_name="$job_name";;
  *)         queue_name="G$job_name";;
esac

# find location of the com file
com_dir=`find $HOME -name $com_name -print`
if [ -z "$com_dir" ]; then
  echo "gaussian job simulation file not found, no file named: $com_name"
  exit 1
fi

if [ `echo "$com_dir" | wc -l` -ne 1 ]; then
  echo "gaussian job simulation file name not unique, found multiple .com files:"
  echo "$com_dir"
  exit 1
fi

# change current working directory to the location of the com file
com_dir=`dirname $com_dir`
cd $com_dir

# calculate slots to request, based on num_nodes user desires we run job on
num_slots=$(( num_nodes * 16 ))

# create the qsub file from provided information
qsub_name="$job_name.qsub"
cat > "$qsub_name" <<EOF
#!/bin/bash
#
#$ -cwd
#$ -j y
#$ -S /bin/bash
#$ -N $queue_name
#$ -l mem_free=11G
#$ -l excl=true

# Determine com and log file names
JOB_NAME=$job_name
COM_NAME=\$JOB_NAME.com
LOG_NAME=\$JOB_NAME.log

# Prepare for running Gaussian '09
export GAUSS_LFLAGS=' -vv -opt "Tsnet.Node.lindarsharg: ssh"'
LINDAWORKERS=\$(cat \$PE_HOSTFILE | grep -v "catch_rsh" | awk -F '.' '{print \$1}' | tr '\n' ',' | sed 's/,$//')
echo \${LINDAWORKERS}

# Prepend input deck with necessary information to run Gaussian '09 with Linda 8.2
# Run Gaussian '09
date
(echo %nprocshared=8; echo %lindaworkers=\${LINDAWORKERS}; echo %mem=11GB; cat \${COM_NAME} | grep -v "%nprocs" | grep -v "%lindaworkers" | grep -v "%mem") | \
  /share/apps/g09/g09 > \${LOG_NAME} 
date
EOF

# now run the qsub command
cmd="qsub -pe gauss $num_slots $qsub_name"
echo "Submitting com job file: $com_name"
echo "        number of nodes: $num_nodes (total slots=$num_slots)"
echo "          log file name: $log_name"
echo "              qsub name: $qsub_name"
echo "      working directory: $com_dir"
echo "                command: $cmd"

out=`$cmd`
if [ $? = 0 ]; then
  echo -e "$out$"
  echo -e "\033[32mSubmitted successfully\033[39m"
else
  echo -e "\033[31mError, job failed\033[39m"
fi
sleep 2
echo "$ qstat"
qstat