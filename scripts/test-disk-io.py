#!/usr/bin/env python
"""Test disk I/O speed and performance.  This script is given a location to run from, and parameters
to use.  The script uses the dd tool to estimate disk write speeds for multiple large writes of data."""
import os
import re
import subprocess
import sys
import tempfile


def usage():
    print "Usage: %s num_trials bs count oflag fs_loc" % sys.argv[0]
    print "    num_trials Number of times to write blocks, we report this number of trials to stdout"
    print "    bs         The block size of blocks we write"
    print "    count      The number of blocks we write on each trial"
    print "    oflag      The output flag for dd, can be direct,dsync,sync or all"
    print "    fs_loc     The filesystem directory location in which to do the write I/O performance tests"
    sys.exit(0)

if __name__ == '__main__':

    # parse command line arguments
    if len(sys.argv) != 6:
        print "Error: incorrect number of arguments\n"
        usage()
    num_trials = int(sys.argv[1])
    bs = int(sys.argv[2])
    count = int(sys.argv[3])
    oflag = sys.argv[4]
    if not oflag in ["direct", "dsync", "sync", "all"]:
        print "Error: unknown oflag option: %s\n" % oflag
        usage()
    else:
        if oflag == "all":
            oflags = ['direct', 'dsync', 'sync']
        else:
            oflags = [oflag]
    fs_loc = sys.argv[5]

    # try and change to the location to perform tests in
    try:
        os.chdir(fs_loc)
        tempfile.tempdir = fs_loc
    except OSError:
        print "Error: Bad test location: %s\n" % fs_loc
        usage()

    # perform the trials with dd
    for oflag in oflags:
        for trial in range(1, num_trials+1):
            tmpfile = tempfile.mktemp(".io")
            cmd = ['dd', 'if=/dev/zero', 'of=%s' % tmpfile, 'bs=%d' % bs, 'count=%d' % count, 'oflag=%s' % oflag]
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            out, _none_ = p.communicate()
            status = p.returncode
            os.remove(tmpfile)

            # return status 0 means successfully ran command
            if status == 0: 
                match = re.search('(.*) bytes .* copied, (.*) s, (.*) .*/s', out)
                if match:
                    size = int(match.group(1))
                    time = float(match.group(2))
                    dd_rate = float(match.group(3))
                    rate = float(size) / time 
                    print "%d, %s, %d, %f, %f, %f, %s" % (trial, oflag, size, time, rate, dd_rate, tmpfile)
