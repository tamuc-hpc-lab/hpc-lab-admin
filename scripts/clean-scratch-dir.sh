#!/usr/bin/env bash
# Script to clean all files in /state/partition1 (scratch directorys) that are older than 2 days old.

unset PATH   # avoid accidental use of $PATH

# ----- commands used by this script
ID=/usr/bin/id
ECHO=/bin/echo
FIND=/bin/find
XARGS=/usr/bin/xargs
RM=/bin/rm

# ----- file locations
SCRATCH_DIR=/state/partition1

# ------ the script itself

# make sure we're running as root
if (( `$ID -u` != 0 )); then
    $ECHO "Sorry, must be root.  Exiting..."
    exit
fi

# find files older than 2 days, and remove them
del_files=`$FIND $SCRATCH_DIR -mtime +1 -type f -print | $XARGS $ECHO $1`
for file in $del_files;
do
    $RM -f $file
done

