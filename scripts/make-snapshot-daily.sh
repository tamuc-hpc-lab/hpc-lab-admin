#!/usr/bin/env bash
#---------------------------------------------------------
# Create a rotating daily snapshot of the user's home data
#----------------------------------------------------------

unset PATH # avoid accidental use of $PATH

# --------------- system commands used by this script
ID=/usr/bin/id
ECHO=/bin/echo

MOUNT=/bin/mount
RM=/bin/rm
MV=/bin/mv
CP=/bin/cp
TOUCH=/bin/touch

RSYNC=/usr/bin/rsync

# -------------- file locations

MOUNT_DEVICE=/dev/sdb1
BACKUP_DIR=/backup
SNAPSHOT_RW=/backup/snapshot
EXCLUDES=/root/backup_exclude

# -------------- the script itself

# make sure we're running as root
if (( `$ID -u` != 0 )); then { $ECHO "Sorry, must be root.  Exiting..."; exit; } fi

# attempt to remount the RW mount point as RW; else abort
$MOUNT -o remount,rw $MOUNT_DEVICE $BACKUP_DIR
if (( $? )); then
    $ECHO "snapshot: could not remount $SNAPSHOT_RW readwrite"
    exit
fi

# rotating snapshots of /export/home

# step 1: delete the oldest snapshot, if it exists:
if [ -d $SNAPSHOT_RW/home/daily.7 ]; then
    $RM -rf $SNAPSHOT_RW/home/daily.7
fi

# step 2: shift the middle snapshot(s) back by one, if they exist
if [ -d $SNAPSHOT_RW/home/daily.6 ]; then
    $MV $SNAPSHOT_RW/home/daily.6 $SNAPSHOT_RW/home/daily.7
fi

if [ -d $SNAPSHOT_RW/home/daily.5 ]; then
    $MV $SNAPSHOT_RW/home/daily.5 $SNAPSHOT_RW/home/daily.6
fi

if [ -d $SNAPSHOT_RW/home/daily.4 ]; then
    $MV $SNAPSHOT_RW/home/daily.4 $SNAPSHOT_RW/home/daily.5
fi

if [ -d $SNAPSHOT_RW/home/daily.3 ]; then
    $MV $SNAPSHOT_RW/home/daily.3 $SNAPSHOT_RW/home/daily.4
fi

if [ -d $SNAPSHOT_RW/home/daily.2 ]; then
    $MV $SNAPSHOT_RW/home/daily.2 $SNAPSHOT_RW/home/daily.3
fi

if [ -d $SNAPSHOT_RW/home/daily.1 ]; then
    $MV $SNAPSHOT_RW/home/daily.1 $SNAPSHOT_RW/home/daily.2
fi

# step 3: make a hard-link-only (except for dirs) copy of the latest snapshot
# if that exists
if [ -d $SNAPSHOT_RW/home/daily.0 ]; then
    $CP -al $SNAPSHOT_RW/home/daily.0 $SNAPSHOT_RW/home/daily.1
fi

# step 4: rsync from the system into the latest snapshot (notice that
# rsync behaves like cp --remove-destination by default, so the destination
# is unlinked first.  If it were not so, this would copy over the other
# snapshot(s) too!
$RSYNC                                                        \
        -va --delete --delete-excluded                       \
        --exclude-from="$EXCLUDES"                            \
        /export/home/ $SNAPSHOT_RW/home/daily.0

# step 5: update the mtime of daily.0 to reflect the snapshot time
$TOUCH $SNAPSHOT_RW/home/daily.0

# now remount the RW snapshot mountpoint as readonly

$MOUNT -o remount,ro $MOUNT_DEVICE $BACKUP_DIR
if (( $? )); then
    $ECHO "snapshot: could not remount $SNAPSHOT_RW readonly"
    exit
fi
